const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let y 
let x 

const mazeEl = document.getElementById('maze')

const createMaze = function (blueprint){
    for(let rowNum = 0; rowNum < blueprint.length; rowNum++){
        const rowString = blueprint[rowNum]
        let blockdiv = ""

        for(let colNum = 0; colNum < rowString.length; colNum ++){
            const blockType = rowString[colNum]
            if (blockType === "S"){
                blockdiv += '<div class="block start"></div>'
                y = rowNum
                x = colNum
            }else if(blockType === "W" ){
                blockdiv += '<div class="block wall"></div>'
             } else if (blockType === "F"){
                blockdiv += '<div class="block finish"></div>'
            } else {
                blockdiv += '<div class="block"></div>'
            }
        }

        mazeEl.innerHTML += `<div class="row">${blockdiv}</div>`
    }
}

createMaze(map)
  let playerDiv = document.createElement('div')
  playerDiv.id = 'player'
//   let playerDiv = document.getElementById('player')
  document.querySelector('.start').appendChild(playerDiv)
document.addEventListener('keydown' , movePlayer)

let playerUpDn = 306;
let playerLtRt = 10;
let mazeWall = 9;
let mazeCol = 0;
function movePlayer(event) {
    if(event.keyCode === 38){
        if(map[mazeWall-1][mazeCol]!== "W"){
            playerUpDn -= 25
            mazeWall -= 1
            document.getElementById('player').style.top = (playerUpDn) + 'px'
        }
        

    }else if (event.keyCode === 40) {
        if(map[mazeWall+1][mazeCol]!== "W"){
             playerUpDn += 25
             mazeWall += 1
            document.getElementById('player').style.top = (playerUpDn) + 'px'
        }
       

    }else if (event.keyCode === 37) {
        if(map[mazeWall][mazeCol-1]!== "W"){
            let job = mazeCol -1
            if((job) >= 0){
                playerLtRt -= 25
                mazeCol -= 1
                document.getElementById('player').style.left = (playerLtRt) + 'px'
            }
           
 
        }
        

    }else if (event.keyCode === 39) {
        if(map[mazeWall][mazeCol+1]!== "W"){
            let job2 = mazeCol+1
            if((job2) <= 20){
                playerLtRt += 25
                mazeCol += 1
                document.getElementById('player').style.left = (playerLtRt) + 'px'
            }
            
        }
     if(map[mazeWall][mazeCol]== "F"){
         let endgame = document.createElement('h1')
         endgame.innerHTML = 'U won!'
         document.body.appendChild(endgame)
     }   
    }
console.log(mazeWall)
console.log(mazeCol)
}

